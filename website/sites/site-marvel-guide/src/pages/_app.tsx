import { type Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import { type AppType } from "next/app";

import { ClientContextProvider } from "@whitelotus/front-shared";

import "~/styles/globals.css";
import { makeMarvelGuideClientContext } from "../app";

const MarvelApp: AppType<{ session: Session | null }> = ({
  Component,
  pageProps: { session, ...pageProps },
}) => {
  const context = makeMarvelGuideClientContext();

  return (
    <SessionProvider session={session}>
      <ClientContextProvider value={context}>
        <Component {...pageProps} />
      </ClientContextProvider>
    </SessionProvider>
  );
};

export default MarvelApp;
