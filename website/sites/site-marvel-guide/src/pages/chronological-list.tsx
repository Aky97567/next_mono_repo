import type { GetServerSidePropsContext } from "next";
import { MarvelGuideServerSideRequest } from "@whitelotus/back-runtime-marvelguide";
import { MarvelGuide } from "@whitelotus/front-pages";

export const getServerSideProps = (nextContext: GetServerSidePropsContext) => {
  const handler = new MarvelGuideServerSideRequest();
  return handler.getServerSideProps(nextContext);
};

export default MarvelGuide.ChronologicalListPage;
