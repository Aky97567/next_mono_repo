type MarvelGuideClientContext = Record<string, Record<string, unknown>>;

const makeMarvelGuideClientContext = (): MarvelGuideClientContext => {
  return {};
};

export { makeMarvelGuideClientContext };
