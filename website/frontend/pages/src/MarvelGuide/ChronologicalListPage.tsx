import React from 'react';
import { Placeholder } from './Placeholder';

const ChronologicalListPage = (props: any) => {
  const { content } = props;

  if (!content) {
    // return null;
    return <Placeholder {...props} />;
  }

  return <Placeholder {...props} />;
};

export { ChronologicalListPage };
