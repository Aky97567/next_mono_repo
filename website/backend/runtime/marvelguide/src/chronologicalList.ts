
import {
    ExecutionContext,
    NextServerSideRequestHandler,
  } from '@whitelotus/lib-ext-next';


import { ApplicationDependencyInitializer } from './initialization';

type MarvelGuideView = {}
type Controllers = {}
type Services = {}
type Query = {}

type ChronologicalListPageContext = ExecutionContext<
MarvelGuideView,
  Controllers,
  Services,
  Query
>;

class MarvelGuideServerSideRequest extends NextServerSideRequestHandler<MarvelGuideView, Controllers, Services, Query>  {
    constructor(locale?: string) {
      super('Dynamic Landing Page', new ApplicationDependencyInitializer());
    }
    protected async executeRequest(context: ChronologicalListPageContext) {
        return context.response.ok({})
    }
}

export {MarvelGuideServerSideRequest}